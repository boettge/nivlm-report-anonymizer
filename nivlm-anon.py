#!/usr/bin/env python3
#==============================================================================
# Anonymization of NIVLM log data
#
# J. Boettge <boettge@mpi-halle.mpg.de>  2022-10-19 - 2023-10-02
#==============================================================================
#
# How Can I Export a Usage Log in NI VLM 3.1 to Generate Custom Reports?
# https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z000000kKh7SAE
#
# Dataset fiels:
# Index   Field
# -----   -----
#  0      Date/Time
#  1      Action
#  2      User
#  3      Host
#  4      Vendor
#  5      (unknown)
#  6      License name
#  7      Type (computer or user based)
#  8      License version
#  9      Product
# 10      Product version
# 11      Location
# 12      (empty)
# 13      Group
# 14      (empty)
#==============================================================================

import sys
import os
import re
import json
#import binascii
from hashlib import pbkdf2_hmac
import dateutil.tz as dtz
from datetime import datetime as dt
import pandas as pd

DBG = True
#LOGFILE='/tmp/mpi/nivlm.log'

HOSTPREFIX='host_'
USERPREFIX='user_'
FILESUFFIX='_anon'
LOCATION= 'Halle (Saale), SA, Germany'


HASH_FUNCTION = 'pbkdf2_hmac' 
HASH_DIGEST   = 'sha256'

SALTLEN = 16      # length of the salt for the password-based key derivation function
ITERS   = 10_000  # number of iterations for the password-based key derivation function
HASHLEN = 10      # the hash string has 64 digits; we don't need that much

#----------------------------------------
def anonymizeLog(logfile):
#----------------------------------------
    global aLOG
    now = dt.now()
    local_tz = dtz.tzlocal()
    now = now.replace(tzinfo = local_tz)
    now_str = dt.strftime(now,'%Y-%m-%d %H:%M:%S %z')
    
    # read log and split into table
    print("\n" + 50*"=", 'Reading file ', 50*"=")
    with open(logfile, encoding='utf-8-sig') as f:
        LOG = f.read().splitlines()

    aLOG=[]
    for l in LOG:
        if re.match(r'^\s*$', l): continue
        a=l.split("\t")
        aLOG.append(a)
    LOG.clear()
    print("Log file contains {:d} entries.".format(len(aLOG)))

    # lists of unique users and hosts
    lHosts=[]
    lUsers=[]
    for l in aLOG:
        if len(l)<4: continue
        if not (l[2] in lUsers): lUsers.append(l[2])
        if not (l[3] in lHosts): lHosts.append(l[3])
    print("User count: {:d}".format(len(lUsers)))
    print("Host count: {:d}".format(len(lHosts)))

    # build anonymization table
    if DBG: print("\n" + 50*"=", 'Users', 58*"=")
    dUsers={}
    for u in lUsers:
        salt = os.urandom(SALTLEN)
        hash = pbkdf2_hmac(HASH_DIGEST, bytes(u,'UTF-8'), salt, ITERS)
        anon = USERPREFIX+hash.hex()[:HASHLEN]
        if DBG: print("{:16s}{:16s}{:65s}{:65s}".format(u, anon, hash.hex(), salt.hex()))
        dUsers[u] = {'anon':anon, HASH_FUNCTION:hash.hex(), 'salt':salt.hex()}
    if DBG: print("\n" + 50*"=", 'Hosts', 58*"=")
    dHosts={}
    for h in lHosts:
        salt = os.urandom(SALTLEN)
        hash = pbkdf2_hmac(HASH_DIGEST, bytes(h,'UTF-8'), salt, ITERS)
        anon = HOSTPREFIX+hash.hex()[:HASHLEN]
        if DBG: print("{:16s}{:16s}{:65s}{:65s}".format(h, anon, hash.hex(), salt.hex()))
        dHosts[h] = {'anon':anon, HASH_FUNCTION:hash.hex(), 'salt':salt.hex()}
    
    # replace user and host with anonymization:
    for l in aLOG:
        l[2] = dUsers[l[2]]['anon']
        l[3] = dHosts[l[3]]['anon']

    # add location as requested in documentation
    for l in aLOG:
        l[11] = LOCATION
    
    # output file names
    (fDir,fLogName)=os.path.split(LOGFILE)
    fNS=os.path.splitext(fLogName)
    fNS=(fNS[0]+FILESUFFIX, fNS[1])
    outfile = os.path.join(fDir,"".join(fNS))
    anonfile = os.path.join(fDir,"".join((fNS[0],'.json')))
    print("\n" + 50*"=", 'Writing files', 50*"=")
    print("Anonymized log file     : {:s}".format(outfile))
    print("Anonymization table file: {:s}".format(anonfile))
    
    # write anonymized logfile
    aLines=[]
    for i in range(len(aLOG)): aLines.append('\t'.join(aLOG[i]) + '\n')
    with open(outfile, 'wt') as f:
        f.writelines(aLines)
    
    # write JSON file with anonymization table
    jHashParams = {'function': HASH_FUNCTION, 'digest': HASH_DIGEST, 'params': {'iterations' : ITERS}}
    JSON = {'date': now_str,
            'original log' : logfile,
            'anonymized log' : outfile,
            'script': os.path.abspath(__file__),
            'hash': jHashParams,
            'users': dUsers,
            'hosts': dHosts}
    with open(anonfile, 'wt', encoding ='utf8') as f:
        json.dump(JSON, f, indent=4, sort_keys=False, )

    # export to XLSX:
    fNS=os.path.splitext(fLogName)
    fNS=(fNS[0]+FILESUFFIX, ".xlsx")
    xlsxfile = os.path.join(fDir,"".join(fNS))
    print("Anonymization XLSX file : {:s}".format(xlsxfile))

    print("* fixing date/time format")
    for l in aLOG:
        ts = dt.strptime(l[0], u'%m/%d/%y %H:%M:%S')
        l[0]=ts.strftime("%Y-%m-%d %H:%M:%S")

    print("* building data frame")
    aColumns = ['datetime', 'action', 'user', 'host', 'vendor', 'unknown', 'license_name', 'license_model', 'version', 'product', 'product_version', 'location', 'empty1', 'department','empty2' ]
    table=pd.DataFrame(aLOG, columns=aColumns,index=range(len(aLOG)))

    table = table.drop(labels=['empty1','empty2'], axis=1)
    # table = table.sort_values(by=['datetime'])
    print("* writing XLSX file")
    table.to_excel(xlsxfile, sheet_name='NI usage report', header=True, index=False, engine='openpyxl')



if __name__ == '__main__':
    if len(sys.argv) == 2:
        if os.path.isfile(sys.argv[1]):
            LOGFILE = sys.argv[1]
        else:
            print('\nERROR: File not found!\n')
            sys.exit(1)
    else:
        print('\nERROR: Expecting one file as argument!\n')
        sys.exit(2)

    anonymizeLog(LOGFILE)

