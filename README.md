# NI VLM LabView Report Anonymisierung

Die MPG ist im Rahmen des Enterprise Agreements verpflichtet, quartalsweise (**anonymisierte**)
Nutzungszahlen an NI zu uebermitteln.  
Die Software Licensing Group fordert daher in regelmaessigen Abstaenden um die entsprechenden
Reports an, die jeweils mittels Lizenzserver generiert werden koennen.

Die Erstellung der Logs ist im [NI Knowledge Base Artikel](https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z000000kKh7SAE&l=de-DE)
beschrieben.

Der exportierte Report muss jedoch noch anonymisiert werden, bevor er an NI (bzw. zunaechst
die MPDL (soli@mpdl.mpg.de)) gesendet wird.  
Hierfuer wird eine kurze  ["Anleitung"](Documentation_NIVLM_3.1_Manual_Usage.pdf) zur
Verfuegung gestellt...

Das vorliegende Python-Skript uebernimmt diese Aufgabe.


## Erstellung des Logfiles auf dem Lizenz-Server

**Wichtig:** Das GUI des *NI Volume License Manager* darf fuer die Erstellung des
Logfiles nicht laufen! Andernfalls passiert beim Aufruf von `nivlm.exe` mit den erforderlichen
Parametern einfach nichts; es gibt auch keine Fehlermeldung.

In der Regel duerfte der Lizenzmanager im Standardpfad installiert sein. Daher reicht
es in dem Fall im nachfolgenden Beispiel den Pfad fuer das Logfile und den Reportzeitraum
anzupassen.  
Es wird hier die `powershell` verwendet. Fuer die klassische `cmd.exe` ist der Befehl
anzupassen.

```powershell
& "C:\Program Files (x86)\National Instruments\Volume License Manager\nivlm.exe" `
    /CreateLegacyUsageLogPath 'C:\Users\Administrator\Documents\NIVLM-Logs\nivlm_2023q1.log' `
    /startDate 2023-01-01 /endDate 2023-03-31
```


## Requirements

Das Python-Skript benoetigt:

* pandas
* openpyxl

(siehe `requirements.txt`)


## Anwendung

```sh
nivlm-anon.py nivlm.log
```

Ausgabe:
- `nivlm_anon.log`  – der anonymisierte Report
- `nivlm_anon.xlsx` – der anonymisierte Report im Excel-Format
- `nivlm_anon.json` – Metadaten mit
    - Informationen ueber Hash-Verfahren
    - Uebersetzungstabelle User
    - Uebersetzungstabelle Hosts

Das JSON-File ist nicht(!) zur Weiterleitung bestimmt.

-----
Jens Boettge <<boettge@mpi-halle.mpg.de>>, 2023-10-02

